const express = require('express')
const nodemailer = require('nodemailer')
require('dotenv').config()
const homeRouter = require('./src/routes/home');
const path = require('path');
const expressServer = require('debug')('express:server');

const app = express()
const port = 3001

app.use(express.json())
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, '/public')));

app.use('/', homeRouter);

app.listen(port, () => {
    expressServer(`listenning on port ${port}`)
})

app.get('*', (req, res) => {
    res.sendFile('404.html', {
        root: 'public'
    })
})
module.exports = app;