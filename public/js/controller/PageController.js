
export class PageController {
    /**
     * @type { Array<{
     *  eventType: string,
     *  target: Element | null,
     *  function: Function
     * }>
     * }
     */
    handlerList
    constructor(renderer) {
        this.renderer = renderer
    }

    init() {
        this.handlerList.forEach(handler => {
            handler.target.addEventListener(handler.eventType, handler.function);
        })
        document.addEventListener('contextmenu', event => event.preventDefault());
    }

    /**
     * @param {HTMLCollection} htmlCollection
     */
    formatBody(htmlCollection) {
        return Array.from(htmlCollection).reduce((acc, currentLine) => {
            if(currentLine.tagName === 'INPUT') {
                acc[currentLine.name] = currentLine.value
            }
            return acc;
        }, {})
    }
}

