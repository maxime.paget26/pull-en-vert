import CommentSchema from "../model/CommentSchema.js";
import { Fetcher } from "../services/Fetcher.js";
import * as Validator from "../services/Validator.js";
import { PageController } from "./PageController.js";

export class HomePageController extends PageController {
    constructor(renderer) {
        super(renderer);
        this.handlerList = [
            {
                eventType: 'submit',
                target: document.querySelector("#registration"),
                function: this._validateRegistration.bind(this)
            }, {
                eventType: 'submit',
                target: document.querySelector(".contact"),
                function: this._sendComment.bind(this)
            }, {
                eventType: 'change',
                target: document.querySelector('.contact'),
                function: this.checkComment.bind(this)
            }
        ]
    }

    /**
     * 
     * @param {Event} event 
     */
    async checkComment(event) {
        const commentButton = document.querySelector('.commentary__button');
        const valid = Validator.check(event.target.name).fromSchema(CommentSchema);
        console.log(Validator.check(event.target.name).number().minimum(10))
        commentButton.disabled = !valid;
        if(!valid) {
            if(!event.target.classList.contains('error')) {
                event.target.classList.add('error');
            }
        } else {
            event.target.classList.remove('error')
        }
    }

    async _sendComment(event) {
        event.preventDefault();
        const body = this.formatBody(event.target);
        await Fetcher.sendComment(body);
    }

 
    // fonction qui permet d'envoyer un requête http post sur la route de l'inscription
    // et de gérer les erreurs générés
    async _validateRegistration(event) {
        event.preventDefault()
        const { data, status } = await Fetcher.register(event.target);
        // fetch("http://localhost:3000/registration", {
        //     method: "post",
        //     headers: {
        //         'Accept': 'application/json, text/plain, */*',
        //         'Content-Type': 'application/json'
        //     },
        //     body: JSON.stringify({ mail: mailValue })
        // })
        //     .then(async (response) => {
        //         response.json().then(data => {
        //             let inputMail = document.querySelector("#mail")
        //             if (!data.success) {
        //                 if (document.querySelector("#mailError") === null) {
        //                     let p = document.createElement('p')
        //                     p.setAttribute("id", "mailError")
        //                     p.innerText = data.msg
        //                     p.style.color = "red"
        //                     inputMail.blur()
        //                     inputMail.style.border = "1px solid red"
        //                     registrationForm.append(p)
        //                 }
        //             }
        //             else {
        //                 if (document.querySelector("#mailError") != null) {
        //                     document.querySelector("#mailError").remove()
        //                 }
        //                 alert("Merci de vous être inscrit sur Pull en Vert")
        //             }
        //         })
        //     }).catch(error => { console.log("error", error) })
    }
} 

