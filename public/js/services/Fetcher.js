export const Fetcher = {

    sendComment (data) {
        return new Promise((resolve, reject) => {
            fetch("http://localhost:3000/comment", {
                method: "post",
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }).then(async (response) => {
                resolve({
                    data: await response.json(),
                    status: response.status
                })
            }).catch(err => console.error(err)); 
        })
    },

    register (form) {
        return new Promise((resolve, reject) => {
            fetch("http://localhost:3000/contact", {
                method: "post",
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(formatBody(form.elements))
            }).then(async (response) => {
                resolve({
                    data: await response.json(),
                    status: response.status
                })
            }).catch(err => console.error(err)); 
        })
    },

    fetchLang(lang) {
        return new Promise((resolve) => {
            fetch(`js/lang/${lang}.json`, {
                method: "get",
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                }
            }).then(async (response) => {
                resolve({
                    data: await response.json(),
                    status: response.status
                })
            }).catch(err => console.error(err)); 
        })
    }
}