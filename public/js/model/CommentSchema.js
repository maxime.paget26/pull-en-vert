const CommentSchema = {
    type: 'object',
    properties: {
        firstname: {
            type: 'string', minLength: 1
        },
        name: {
            type: 'string', minLength: 1
        },
        phoneNumber: {
            type: 'string',
            minLength: 10,
            maxLength: 10
        },
        content: {
            type: 'string',
            minLength: 1
        }
    },
    required: ['firstname', 'name', 'phoneNumber', 'content']
};

export default CommentSchema