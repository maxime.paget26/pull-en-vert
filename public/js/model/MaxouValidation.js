export default MaxouValidation = {
    number () {
        (function validate(value) {
            if (typeof value !== 'number') {
                this.errors = {
                    code: "type",
                    message: "must be an integer",
                }
            }
        })();
        const opt = {
            minimum(minimum) {
                (function validate(value) {
                    if (value < minimum) {
                        this.errors = {
                            code: "range",
                            message: `must be greater than ${minimum}`,
                        }
                    }
                })();
                return opt
            }
        }
        return false;
    },

    string: {
        error: {
            code: "type",
            message: "must be a string"
        },
        opt: {
            minLength(value) {
                return MaxouValidation.string.opt
            }
        },
        validate(value) {
            
        }
    }
}