import { Fetcher } from "./services/Fetcher.js";
import { HomePageController, PageRenderer} from "./index.js";

const App = {
    controllers: {
        homePage: HomePageController
    },
    renderers: {
        pages: PageRenderer
    },

    async init() {
        App.renderers.pages = new PageRenderer()
        App.controllers.homePage = new HomePageController();
        const { data } = await Fetcher.fetchLang('fr');
        document.documentElement.innerHTML = document.documentElement.innerHTML.replace(/{{(.*?)}}/g, (match) => {
            return data[match.split(/{{|}}/).filter(Boolean)[0].trim()] || ''
        });
        App.controllers.homePage.init()
    }
}

App.init()