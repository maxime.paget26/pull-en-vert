const nodemailer = require('nodemailer');

module.exports = {
    async send(emails) {

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
                user: process.env.USER_AUTH,
                pass: process.env.PASS_AUTH
            }
        })
    
        // send mail with defined transport object
        let info = await transporter.sendMail({
            from: '"Pull en Vert 👻" <pull.envert@gmail.com', //sender address
            to: emails, // list of receivers
            subject: "Hello ✔", // Subject line
            html: "<h1>Hello world?</h1>", // html body
        });
    }
}

