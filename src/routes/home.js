const express = require('express')
const router = express.Router();
const CommentSchema = require('../models/commentSchema');
const Ajv = require('ajv');
const Mailer = require('../services/Mailer');
router.post('/registration', (req, res) => {
    let errors = validationResult(req)
    if (errors.isEmpty()) {
        this.sendEmail(req.body.mail)
        res.send({ msg: "registration successfully", success: "true" })
    } else {
        res.send(JSON.stringify(errors.array()[0]))
    }
})

router.post('/comment',
    async (req, res) => {
        const ajv = new Ajv({ allErrors: true });
        const validation = ajv.compile(CommentSchema);
        if(!validation(req.body)) {
            res.status(400).json({ requestErrors: validation.errors })
        }
    }
)


module.exports = router